.section .text
.arm

.global disable_data_cache
.type disable_data_cache, %function
disable_data_cache:
	mrc p15, 0, r0, c1, c0, 0
	bic r0, r0, #0b100
	mcr p15, 0, r0, c1, c0, 0
	bx lr
	
.global enable_data_cache
.type enable_data_cache, %function
enable_data_cache:
	mrc p15, 0, r0, c1, c0, 0
	orr r0, r0, #0b100
	mcr p15, 0, r0, c1, c0, 0
	bx lr
	
.global disable_MMU
.type disable_MMU, %function
disable_MMU:
	mrc p15, 0, r1, c1, c0, 0
	bic r1, r1, #0b1
	mcr p15, 0, r1, c1, c0, 0
	mov pc, r0
	bx lr
	
.global enable_MMU
.type enable_MMU, %function
enable_MMU:
	mrc p15, 0, r1, c1, c0, 0
	orr r1, r1, #0b1
	mcr p15, 0, r1, c1, c0, 0
	mov pc, r0
	bx lr

.global disable_IRQ
.type disable_IRQ, %function
disable_IRQ:
	mrs r0, cpsr
	orr r0, r0, #0x80  @IRQ
	msr cpsr_c, r0
	bx lr
	
.global disable_FIQ
.type disable_FIQ, %function
disable_FIQ:
	mrs r0, cpsr
	orr r0, r0, #0x40  @FIQ
	msr cpsr_c, r0
	bx lr
	
.global enable_IRQ
.type enable_IRQ, %function
enable_IRQ:
	mrs r0, cpsr
	bic r0, r0, #0x80  @IRQ
	msr cpsr_c, r0
	bx lr

.global enable_FIQ
.type enable_FIQ, %function
enable_FIQ:
	mrs r0, cpsr
	bic r0, r0, #0x40  @FIQ
	msr cpsr_c, r0
	bx lr

.global CleanAndInvalidateEntireDataCache
.type CleanAndInvalidateEntireDataCache, %function
CleanAndInvalidateEntireDataCache:
	mov r0, #0
	mcr p15, 0, r0, c7, c14, 0
	bx lr

.global CleanEntireDataCache
.type CleanEntireDataCache, %function
CleanEntireDataCache:
	mov r0, #0
	mcr p15, 0, r0, c7, c6, 0
	bx lr

.global InvalidateEntireDataCache
.type InvalidateEntireDataCache, %function
InvalidateEntireDataCache:
	mov r0, #0
	mcr p15, 0, r0, c7, c10, 0
	bx lr

.global InvalidateEntireInstructionCache
.type InvalidateEntireInstructionCache, %function
InvalidateEntireInstructionCache:
	mov r0, #0
	mcr p15, 0, r0, c7, c5, 0
	bx lr
	
.global DataMemoryBarrier
.type DataMemoryBarrier, %function
DataMemoryBarrier:
	mov r0, #0
	mcr p15, 0, r0, c7, c10, 5
	bx lr

.global SleepThread
.type SleepThread, %function
SleepThread:
	svc 0x0A
	bx lr
	
.global GetSystemTick
.type GetSystemTick, %function
GetSystemTick:
	svc 0x28
	bx lr
