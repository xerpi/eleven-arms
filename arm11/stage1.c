#include "3ds.h"
#include "utils.h"

#define MAGIC_ADDR	((volatile void*)0xEFFFFFF8)
#define MAGIC_INT   (*(volatile int*)MAGIC_ADDR)
#define FCRAM_PA    ((void *)0x20000000)


/*
#define FB_TOP_LEFT1  (0xF0184E60)
#define FB_TOP_LEFT2  (0xF01CB370)
#define FB_TOP_RIGHT1 (0xF0282160)
#define FB_TOP_RIGHT2 (0xF02C8670)
#define FB_BOT_1	  (0xF02118E0)
#define FB_BOT_2	  (0xF0249CF0)
#define FB_TOP_SIZE	  (0x46500)
#define FB_BOT_SIZE	  (0x3BC40)

#define LCD_FB_PDC0 ((volatile void *)0xFFFCE400)
#define LCD_FB_PDC1 ((volatile void *)0xFFFCE500)
#define LCD_FB_SIZE_OFFSET    0x5C
#define LCD_FB_A_ADDR_OFFSET  0x68
#define LCD_FB_FORMAT_OFFSET  0x70
#define LCD_FB_SELECT_OFFSET  0x78
#define LCD_FB_STRIDE_OFFSET  0x90
#define LCD_FB_B_ADDR_OFFSET  0x94

struct lcd_fb_size {
	uint16_t width;
	uint16_t height;
} __attribute__((packed));

struct lcd_fb_addr {
	volatile void *first;
	volatile void *second;
} __attribute__((packed));

void ClearScreen();*/


//APPCORE code
int main()
{
	//Tell ARM9 we are here!
	MAGIC_INT = 1;
	
	while (MAGIC_INT != 2) {
		InvalidateEntireInstructionCache();
	}
	//ARM11 exception vectors restored (by the ARM9)
	//And bootloader copied

	MAGIC_INT = 3;
	
	//enable_FIQ();
	//enable_IRQ();
	
	disable_MMU(FCRAM_PA);

	while (1) {
		MAGIC_INT = 50;
	}
	
	
	/*struct lcd_fb_addr *fb_top_addr_left = (void*)LCD_FB_PDC0+LCD_FB_A_ADDR_OFFSET;
	struct lcd_fb_addr *fb_top_addr_right = (void*)LCD_FB_PDC0+LCD_FB_B_ADDR_OFFSET;

	struct lcd_fb_addr *fb_bot_addr_left = (void*)LCD_FB_PDC1+LCD_FB_A_ADDR_OFFSET;
	struct lcd_fb_addr *fb_bot_addr_right = (void*)LCD_FB_PDC1+LCD_FB_B_ADDR_OFFSET;
	
	fb_bot_addr_left->first = fb_top_addr_left->first;
	fb_bot_addr_left->second = fb_top_addr_left->second;*/
	/*asm volatile(
		"mlg:\n"
		"mov r0, #0\n"
		"mov r1, #0\n"
		"mov r2, #0\n"
		"mov r3, #0\n"
		"svc 0x7C\n" //KernelSetState
		"ldr r2, =0xEFFFFFF8\n"
		"str r0, [r2]\n"
		"bl CleanAndInvalidateEntireDataCache\n"
		"b mlg\n"
		: : :  "r0", "r1", "r2", "r3");*/
	
	return 0;
}
/*
void ClearScreen(unsigned int color)
{
	int i;
	for (i = 0; i < FB_TOP_SIZE; i+=4) {
		*(int *)(FB_TOP_LEFT1 + i) = color;
		*(int *)(FB_TOP_LEFT2 + i) = color;
		*(int *)(FB_TOP_RIGHT1 + i) = color;
		*(int *)(FB_TOP_RIGHT2 + i) = color;
	}
	for (i = 0; i < FB_BOT_SIZE; i+=4) {
		*(int *)(FB_BOT_1 + i) = color;
		*(int *)(FB_BOT_2 + i) = color;
	}
}
*/
