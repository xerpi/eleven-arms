#include "draw.h"
#include "libc.h"
#include "utils.h"
#include "3ds.h"

extern void *stage1_bin;
extern int	 stage1_bin_size;
#define	     STAGE1_ADDR	   ((volatile void*)0x1FFF4B40)
#define	     MAGIC_ADDR		   ((volatile void*)0x1FFFFFF8)
#define	     ARM11_EXCVEC_ADDR ((volatile void*)0x1FFF4000)
#define      MAGIC_INT         (*(volatile int*)MAGIC_ADDR)
#define      FCRAM_PA          ((volatile void *)0x20000000)

#define ARM_B(cur,dst) ((0b1110<<28) | (0b101<<25) | (0b0<<24) | ((dst-(cur+8))>>2))

unsigned int excvec_backup[8];

extern void *bootloader_start;
extern void *bootloader_end;


//ARM9 running on SVC mode with IRQs, FIQs, and MPU disabled (caches disabled)
int main()
{	
	ClearScreen();
	DEBUG("Eleven ARMs by xerpi");

	//Backup ARM11 exception vectors
	memcpy(excvec_backup, (void*)ARM11_EXCVEC_ADDR, sizeof(excvec_backup));
	DEBUG("Backed up ARM11 exception vec.");
	
	//Copy stage1 payload
	memcpy((void*)STAGE1_ADDR, &stage1_bin, stage1_bin_size);
	DEBUG("stage1 payload copied");
	
	//Set magic number to 0...
	MAGIC_INT = 0;
	DEBUG("Magic number set to 0");
	
	//Overwrite ARM11 exception vectors with "b stage1_addr"
	DEBUG("Overwriting ARM11 exception vectors...");
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 0) = ARM_B(ARM11_EXCVEC_ADDR+0, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 4) = ARM_B(ARM11_EXCVEC_ADDR+4, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 8) = ARM_B(ARM11_EXCVEC_ADDR+8, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 12) = ARM_B(ARM11_EXCVEC_ADDR+12, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 16) = ARM_B(ARM11_EXCVEC_ADDR+16, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 20) = ARM_B(ARM11_EXCVEC_ADDR+20, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 24) = ARM_B(ARM11_EXCVEC_ADDR+24, STAGE1_ADDR);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 28) = ARM_B(ARM11_EXCVEC_ADDR+28, STAGE1_ADDR);
	
	DEBUG("Waiting for ARM11 control...");
	while (MAGIC_INT == 0) ;
	DEBUG("Got ARM11 control :D");
	
	//Restore arm11 exception vectors
	memcpy((void*)ARM11_EXCVEC_ADDR, excvec_backup, sizeof(excvec_backup));
	DEBUG("ARM11 exception vectors restored");
	//Copy bootloader
	memcpy((void*)FCRAM_PA, &bootloader_start, (u32)&bootloader_end - (u32)&bootloader_start);
	DEBUG("Copied bootloader to %p, size: 0x%X", FCRAM_PA, (u32)&bootloader_end - (u32)&bootloader_start);
	DEBUG("FCRAM[0] = 0x%08X", ((u32*)FCRAM_PA)[0]);
	DEBUG("FCRAM[1] = 0x%08X", ((u32*)FCRAM_PA)[1]);
	DEBUG("FCRAM[2] = 0x%08X", ((u32*)FCRAM_PA)[2]);
	DEBUG("FCRAM[3] = 0x%08X", ((u32*)FCRAM_PA)[3]);
	DEBUG("FCRAM[4] = 0x%08X", ((u32*)FCRAM_PA)[4]);
	
	MAGIC_INT = 2;
	//while (MAGIC_INT != 3) ;
	
	
	while (1) {
		draw_fillrect(10, console_y, 8*12, 20, BLACK);
		font_draw_stringf(10, console_y, GREEN, "%i", MAGIC_INT);
	}
	
	return 0;
}
